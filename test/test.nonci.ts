import { expect, tap } from '@pushrocks/tapbundle';
import { Qenv } from '@pushrocks/qenv';
import * as elasticsearch from '../ts/index.js';

let testElasticLog: elasticsearch.ElasticSearch<any>;

tap.test('first test', async () => {
  testElasticLog = new elasticsearch.ElasticSearch({
    indexPrefix: 'testprefix',
    indexRetention: 7,
    node: 'http://localhost:9200',
    auth: {
      username: 'elastic',
      password: 'YourPassword'
    }
  });
  expect(testElasticLog).toBeInstanceOf(elasticsearch.ElasticSearch);
});

tap.test('should send a message to Elasticsearch', async () => {
  await testElasticLog.log({
    timestamp: Date.now(),
    type: 'increment',
    level: 'info',
    context: {
      company: 'Lossless GmbH',
      companyunit: 'lossless.cloud',
      containerName: 'testcontainer',
      environment: 'test',
      runtime: 'node',
      zone: 'ship.zone',
    },
    message: 'GET https://myroute.to.a.cool.destination/sorare?hello=there',
    correlation: null,
  });
});

tap.start();

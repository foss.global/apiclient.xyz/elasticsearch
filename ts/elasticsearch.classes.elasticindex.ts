import * as plugins from './elasticsearch.plugins.js';
import { ElasticSearch } from './elasticsearch.classes.elasticsearch.js';
import { type ILogPackage } from '@pushrocks/smartlog-interfaces';

import { Stringmap } from '@pushrocks/lik';

export class ElasticIndex {
  private stringmap = new Stringmap();
  private elasticSearchRef: ElasticSearch<any>;

  constructor(elasticSearchInstanceArg: ElasticSearch<ILogPackage>) {
    this.elasticSearchRef = elasticSearchInstanceArg;
  }

  public async ensureIndex(prefixArg: string, indexNameArg: string) {
    if (this.stringmap.checkString(indexNameArg)) {
      return indexNameArg;
    }
    const responseArg = await this.elasticSearchRef.client.cat.indices({
      format: 'json',
      bytes: 'm',
    }).catch(err => {
      console.log(err);
    });

    if (!responseArg) {
      throw new Error('Could not get valid response from elastic search');
    }

    // lets delete indexes that violate the retention
    if (Array.isArray(responseArg.body)) {
      const filteredIndices = responseArg.body.filter((indexObjectArg) => {
        return indexObjectArg.index.startsWith(prefixArg);
      });
      const filteredIndexNames = filteredIndices.map((indexObjectArg) => {
        return indexObjectArg.index;
      });
      await this.deleteOldIndices(prefixArg, filteredIndexNames);
    }

    let index = null;

    if (Array.isArray(responseArg.body)) {
      index = responseArg.body.find((indexItemArg) => {
        return indexItemArg.index === indexNameArg;
      });
    }

    if (!index) {
      await this.createNewIndex(indexNameArg);
    }
    this.stringmap.addString(indexNameArg);
    return index;
  }

  public async createNewIndex(indexNameArg: string) {
    const response = await this.elasticSearchRef.client.indices.create({
      wait_for_active_shards: '1',
      index: indexNameArg,
    });
  }

  public async deleteOldIndices(prefixArg: string, indicesArray: string[]) {
    const todayAsUnix: number = Date.now();
    const rententionPeriodAsUnix: number = plugins.smarttime.units.days(
      this.elasticSearchRef.indexRetention
    );
    for (const indexName of indicesArray) {
      if (!indexName.startsWith(prefixArg)) continue;
      const indexRegex = new RegExp(`^${prefixArg}-([0-9]*)-([0-9]*)-([0-9]*)$`) 
      const regexResult = indexRegex.exec(indexName);
      const dateAsUnix: number = new Date(
        `${regexResult[1]}-${regexResult[2]}-${regexResult[3]}`
      ).getTime();
      if (todayAsUnix - rententionPeriodAsUnix > dateAsUnix) {
        console.log(`found old index ${indexName}`);
        const response = await this.elasticSearchRef.client.indices.delete(
          {
            index: indexName,
          }).catch(err => {
            console.log(err);
          });
      }
    }
  }
}
